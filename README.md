# Getting Started with Create React App
# Local Setup

### Node 18 required
### Node Modules

```bash
yarn
```

# Running the application locally

```bash
yarn install
yarn start
```
