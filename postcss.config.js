const autoprefixer = require('autoprefixer');

module.exports = {
  plugins: [
    autoprefixer
    // Add more PostCSS plugins as needed
  ]
};
