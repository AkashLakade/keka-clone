import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import SignIn from "./components/registrations/SignIn";
import { routerPaths } from "./constants/paths";
import LeaveIndex from "./components/leave/LeaveIndex";
import Landing from "./components/Landing";
import DashboardIndex from "./components/dashboard/DashboardIndex";
import InboxIndex from "./components/InboxIndex";
import MyTeamIndex from "./components/team/MyTeamIndex";
import MyFinanceIndex from "./components/finance/MyFinanceIndex";
import OrganizationIndex from "./components/orgnization/orgIndex";
import AttendanceIndex from "./components/leave/AttendanceIndex";
import EmployeeIndex from "./components/employee/EmployeeIndex";

const Router = () => {
  function requireSignIn(ComponentToRender, routeProps = {}) {
    const isAuthenticated = localStorage.getItem("authenticated") === "true";

    if (isAuthenticated) {
      return <ComponentToRender {...routeProps} />;
    } else {
      return (
        <Redirect
          to={{
            pathname: routerPaths.signIn,
          }}
        />
      );
    }
  }

  return (
    <BrowserRouter>
      <Switch>
        <Route
          path={routerPaths.signIn}
          render={(props) => <SignIn {...props} />}
        />
        <Route
          exact
          path={routerPaths.root}
          render={(props) => <Landing {...props} />}
        />
        <Route
          exact
          path={routerPaths.home}
          render={(props) => requireSignIn(DashboardIndex, props)}
        />
        <Route
          exact
          path={routerPaths.leaveSummary}
          render={(props) => requireSignIn(LeaveIndex, props)}
        />
        <Route
          exact
          path={routerPaths.inbox}
          render={(props) => requireSignIn(InboxIndex, props)}
        />
        <Route
          exact
          path={routerPaths.myTeam}
          render={(props) => requireSignIn(MyTeamIndex, props)}
        />
        <Route
          exact
          path={routerPaths.myfinance}
          render={(props) => requireSignIn(MyFinanceIndex, props)}
        />
        <Route
          exact
          path={routerPaths.org}
          render={(props) => requireSignIn(OrganizationIndex, props)}
        />
        <Route
          exact
          path={routerPaths.attendance}
          render={(props) => requireSignIn(AttendanceIndex, props)}
        />
        <Route
          exact
          path={routerPaths.employee}
          render={(props) => requireSignIn(EmployeeIndex, props)}
        />
      </Switch>
    </BrowserRouter>
  );
};

export default Router;
