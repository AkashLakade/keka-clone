export const routerPaths = {
  root: "/",
  signIn: "/login",
  home: "/home/dashboard",
  me: "/me",
  myTeam: "/myteam/summary/direct",
  inbox: "/inbox/action",
  myfinance: "/myfinances/summary",
  org: "/org/employees/directory",
  leaveSummary: "/me/leave",
  attendance: "/me/attendance",
  employee: "/me",
};
