import React, { useState } from "react";
import moment from "moment/moment";
import { capitalCase } from "../helpers/stringHelpers";
import { Button, Dropdown, Icon } from "semantic-ui-react";

export const LEAVE_HISTORY_COLUMNS = [
  {
    label: "Leave Dates",
    renderCell: (item) => renderDateCell(item),
    renderSortValue: (item) => item.date,
    sortable: true,
  },
  {
    label: "Leave Type",
    renderCell: (item) => item.leaveType,
    renderSortValue: (item) => item.leaveType,
    sortable: true,
  },
  {
    label: "Status",
    renderCell: (item) => renderStatusCell(item),
    renderSortValue: (item) => item.status,
    sortable: true,
  },
  {
    label: "Requested By",
    renderCell: (item) => item.requestedBy,
    renderSortValue: (item) => item.requestedBy,
    sortable: true,
  },
  {
    label: "Action Taken On",
    renderCell: (item) => moment(item.changedOn).format("ll"),
    renderSortValue: (item) => item.changedOn,
    sortable: true,
  },
  {
    label: "Leave Note",
    renderCell: (item) => item.note,
    renderSortValue: (item) => item.note,
    sortable: true,
  },
  {
    label: "Reject/Cancellation Reason",
    renderCell: (item) => item.rejectReason,
    renderSortValue: (item) => item.rejectReason,
    sortable: false,
  },
];

const renderStatusCell = (item) => {
  return (
    <div>
      <p style={{ margin: 0 }}>{capitalCase(item.status)}</p>
      {(item.approvedBy || item.rejectedBy) && (
        <p style={{ margin: 0, color: "#777", fontSize: "12px" }}>
          by {item.approvedBy || item.rejectedBy}
        </p>
      )}
    </div>
  );
};

const renderDateCell = (item) => {
  return (
    <div>
      <p style={{ margin: 0 }}>{moment(item.date).format("ll")}</p>
      <p style={{ margin: 0, color: "#777", fontSize: "12px" }}>
        {item.duration} Day
      </p>
    </div>
  );
};

const renderActionsCell = (item, handleEllipsisClick, isDropdownVisible) => (
  <div>
    <Icon
      name="ellipsis vertical"
      onClick={() => handleEllipsisClick(item.rowIndex)}
    />
    {isDropdownVisible[item.rowIndex] && (
      <div className="actions-menu">
        <Button
          icon="eye"
          onClick={() => console.log("Show Details", item.rowIndex)}
        />
      </div>
    )}
  </div>
);
