import React, { Component } from "react";
import NavbarBg from "../assets/images/nav-bg.png";
import { Image } from "semantic-ui-react";
import SearchComponent from "./shared/elements/SearchComponent";
import ProfileDropdown from "./shared/components/ProfileDropdown";
import DummyProfileImage from "../assets/images/profile-pic.jpeg";

class TopNav extends Component {
  render() {
    return (
      <nav className="navbar page-header fixed-top w-100">
        <div
          className="d-flex justify-content-between w-100"
          style={{ backgroundImage: `url(${NavbarBg})` }}
        >
          <div className="d-flex align-items-center">
            <a className="navbar-brand cursor-pointer" href="#/">
              <div className="position-relative">
                <Image
                  alt="Keka"
                  src="https://cdn.kekastatic.net/shared/branding/logo/keka-logo-light.svg"
                />
              </div>
            </a>
            <div className="nav-title align-self-center">
              <a className="org-name" href="#/">
                {" "}
                Nitor Infotech Private Limited{" "}
              </a>
            </div>
            <SearchComponent />
          </div>
          <div className="d-flex"></div>
          <div className="d-flex align-items-center">
            <div className="page-header-left d-flex align-items-center  ">
              <ProfileDropdown
                profileName={"Akash Lakade"}
                profileImage={DummyProfileImage}
              />
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

export default TopNav;
