import React, { Component } from "react";
import { Redirect } from "react-router";
import { routerPaths } from "../constants/paths";

class Landing extends Component {
  render() {
    const { authenticated } = this.props;

    if (!authenticated) {
      return <Redirect to={routerPaths.signIn} />;
    } else {
      return <Redirect to={routerPaths.me} />;
    }
  }
}

export default Landing;
