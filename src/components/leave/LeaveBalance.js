import React, { Fragment } from "react";
import {
  Card,
  CardContent,
  CardDescription,
  Grid,
  GridColumn,
  GridRow,
} from "semantic-ui-react";
import { MyResponsivePie } from "../stats/ResponsivePie";
import { leaveBalance } from "../../mocks/LeavesData";

const LeaveSection = ({
  id,
  available,
  consumed,
  quota,
  accrued,
  carryover,
}) => {
  const formatValue = (value) => (isFinite(value) ? value : "&infin;");

  const isNumericAvailable = !isNaN(available);

  const shouldShowPieChart =
    (consumed > 0 || consumed === "Infinity") && isNumericAvailable;

  const pieData = shouldShowPieChart
    ? [
        {
          id: "Consumed",
          label: "Consumed",
          value: consumed,
          color: "#79C7E3",
        },
        {
          id: "Available",
          label: "Available",
          value: isNumericAvailable ? available : 0,
          color: "#2986CE",
        },
      ]
    : [];

  const customCenterTextLayer = ({ centerX, centerY }) => (
    <text
      x={centerX}
      y={centerY}
      textAnchor="middle"
      dominantBaseline="middle"
      style={{
        fontSize: "14px",
        fontWeight: "bold",
        fill: "#333333",
        whiteSpace: "break-spaces",
      }}
    >
      <tspan dy="-0.6em">{available} Days</tspan>
      <tspan dy="1.2em" dx={available > 0 ? "-4.3em" : "-3.5em"}>
        Available
      </tspan>
    </text>
  );

  return (
    <GridColumn width={4}>
      <div className="leave-balance-section">
        <div className="balance-card-header-section">
          <p className="card-header justify-content-between">{id}</p>
        </div>
        {shouldShowPieChart ? (
          <div style={{ width: "auto", height: "200px" }}>
            <MyResponsivePie
              data={pieData}
              topMargin={20}
              bottomMargin={20}
              customCenterTextLayer={customCenterTextLayer}
            />
          </div>
        ) : (
          <div
            style={{
              width: "auto",
              height: "200px",
              display: "grid",
              placeItems: "center",
            }}
          >
            No data to display.
          </div>
        )}
        <Grid celled columns="equal">
          <GridRow>
            <GridColumn>
              <span className="meta-text"> Available </span>
              <p dangerouslySetInnerHTML={{ __html: formatValue(available) }} />
            </GridColumn>
            <GridColumn>
              <span className="meta-text"> Consumed </span>
              <p dangerouslySetInnerHTML={{ __html: formatValue(consumed) }} />
            </GridColumn>
          </GridRow>

          <GridRow>
            {accrued && (
              <GridColumn>
                <span className="meta-text"> ACCRUED SO FAR </span>
                <p dangerouslySetInnerHTML={{ __html: formatValue(accrued) }} />
              </GridColumn>
            )}
            {carryover && (
              <GridColumn>
                <span className="meta-text"> CARRYOVER </span>
                <p
                  dangerouslySetInnerHTML={{ __html: formatValue(carryover) }}
                />
              </GridColumn>
            )}
            <GridColumn>
              <span className="meta-text"> ANNUAL QUOTA </span>
              <p dangerouslySetInnerHTML={{ __html: formatValue(quota) }} />
            </GridColumn>
          </GridRow>
        </Grid>
      </div>
    </GridColumn>
  );
};

const LeaveBalance = () => (
  <Fragment>
    <GridRow>
      <GridColumn>
        <p className="leave-header">Leave Balances</p>
      </GridColumn>
    </GridRow>
    <GridRow>
      {leaveBalance.map((section, index) => (
        <LeaveSection key={index} data={section} {...section} />
      ))}
    </GridRow>
    <GridRow>
      <div style={{ padding: "0px 15px", width: "100%" }}>
        <Card fluid>
          <CardContent>
            <CardDescription>
              <div
                style={{ height: "100px" }}
                className="d-flex align-items-center"
              >
                Other Leave Types Available : Optional Leave, Paternity Leave
              </div>
            </CardDescription>
          </CardContent>
        </Card>
      </div>
    </GridRow>
  </Fragment>
);

export default LeaveBalance;
