import React, { Component, Fragment } from "react";
import { GridColumn, GridRow, Icon, Popup } from "semantic-ui-react";
import { MyResponsiveBar } from "../stats/ResponsiveBar";
import {
  consumedLeaveTypesData,
  weeklyLeaveData,
  yearlyLeaveData,
} from "../../mocks/LeavesData";
import { MyResponsivePie } from "../stats/ResponsivePie";

class LeaveStats extends Component {
  customCenterTextLayer = ({ centerX, centerY }) => (
    <text
      x={centerX}
      y={centerY}
      textAnchor="middle"
      dominantBaseline="middle"
      style={{
        fontSize: "14px",
        fontWeight: "bold",
        fill: "#333333",
        whiteSpace: "break-spaces",
      }}
    >
      <tspan dy="-0.6em">Leave</tspan>
      <tspan dy="1.2em" dx="-2.5em">
        Types
      </tspan>
    </text>
  );

  render() {
    return (
      <Fragment>
        <GridRow>
          <GridColumn>
            <p className="leave-header">My Leave Stats</p>
          </GridColumn>
        </GridRow>
        <GridRow>
          <GridColumn width={4}>
            <div className="leave-section">
              <div className="card-header-section">
                <p className="card-header">Weekly Pattern</p>
                <Popup
                  wide
                  trigger={
                    <Icon color="grey" size="small" circular name="info" />
                  }
                  content="This graph indicates your week-over-week pattern of leave utilization during the entire year. 
                                 You can see on what days of the week you were mostly on leave, during the year."
                  size="small"
                />
              </div>
              <div style={{ width: "auto", height: "100px" }}>
                <MyResponsiveBar
                  data={weeklyLeaveData}
                  indexBy="day"
                  usedKey={["value"]}
                  maxValue={10}
                />
              </div>
            </div>
          </GridColumn>
          <GridColumn width={4}>
            <div className="leave-section">
              <div className="card-header-section">
                <p className="card-header">Consumed Leave Types</p>
                <Popup
                  wide
                  trigger={
                    <Icon color="grey" size="small" circular name="info" />
                  }
                  content="See consumed leave type by consumption percentage/share."
                  size="small"
                />
              </div>
              <div style={{ width: "auto", height: "100px" }}>
                <MyResponsivePie
                  data={consumedLeaveTypesData}
                  customCenterTextLayer={this.customCenterTextLayer}
                />
              </div>
            </div>
          </GridColumn>
          <GridColumn width={7}>
            <div className="leave-section">
              <div className="card-header-section">
                <p className="card-header">Monthly Stats</p>
                <Popup
                  wide
                  trigger={
                    <Icon color="grey" size="small" circular name="info" />
                  }
                  content="This graph indicates your month-over-month leave utilization during the entire year.  
                                You can get an overview of number of leave taken in any month during the year."
                  size="small"
                />
              </div>
              <div style={{ width: "auto", height: "100px" }}>
                <MyResponsiveBar
                  data={yearlyLeaveData}
                  indexBy="month"
                  usedKey={["value"]}
                  maxValue={5}
                />
              </div>
            </div>
          </GridColumn>
        </GridRow>
      </Fragment>
    );
  }
}

export default LeaveStats;
