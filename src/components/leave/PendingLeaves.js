import React, { Component } from "react";
import { Card, CardContent, CardDescription, Image } from "semantic-ui-react";
import ElephantImage from "../../assets/images/elephant.png";

class PendingLeaves extends Component {
  render() {
    return (
      <div className="pending-leaves">
        <div className="leave-section">
          <div className="pending-leave-no-leave">
            <Image size="small" src={ElephantImage} />
            <div className="pending-leave-no-leaves">
              <p>Nothing here.</p>
              <p>Working hard yeah?? Request time off on the right -</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PendingLeaves;
