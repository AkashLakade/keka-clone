import React, { Component } from "react";
import {
  Card,
  CardContent,
  CardDescription,
  Dropdown,
  Grid,
  GridColumn,
  GridRow,
  Image,
} from "semantic-ui-react";
import PendingLeaves from "./PendingLeaves";
import LeaveRequest from "./LeaveRequest";
import LeaveStats from "./LeaveStats";
import LeaveBalance from "./LeaveBalance";
import LeaveHistory from "./LeaveHistory";

class LeaveSummary extends Component {
  render() {
    const { request } = this.props;
    const yearOptions = [
      {
        key: "1",
        text: "Apr 2023 - Mar 2024",
        value: "Apr 2023 - Mar 2024",
      },
      {
        key: "2",
        text: "Apr 2022 - Mar 2023",
        value: "Apr 2022 - Mar 2023",
      },
      {
        key: "3",
        text: "Apr 2021 - Mar 2022",
        value: "Apr 2021 - Mar 2022",
      },
    ];
    return (
      <div className="leave-container">
        <Grid>
          <GridRow>
            <GridColumn width={13}>
              <p className="leave-header">Pending leave requests</p>
            </GridColumn>
            <GridColumn width={3}>
              <Dropdown
                className="year-selector"
                placeholder="Select year"
                selection
                options={yearOptions}
                defaultValue="Apr 2023 - Mar 2024"
              />
            </GridColumn>
          </GridRow>
          <GridRow>
            <GridColumn width={13}>
              <PendingLeaves />
            </GridColumn>
            <GridColumn width={3}>
              <LeaveRequest />
            </GridColumn>
          </GridRow>
          <LeaveStats />
          <LeaveBalance />
          <LeaveHistory />
        </Grid>
      </div>
    );
  }
}

export default LeaveSummary;
