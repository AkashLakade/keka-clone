import React, { Component } from "react";
import { Button } from "semantic-ui-react";

class LeaveRequest extends Component {
  render() {
    return (
      <div className="request-leave-container">
        <div className="leave-section">
          <div className="request-leave-btn">
            <Button content={"Request Leave"} primary />
          </div>
          <a className="text-link">Leave Policy Explanation</a>
        </div>
      </div>
    );
  }
}

export default LeaveRequest;
