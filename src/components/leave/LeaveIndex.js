import React, { Component } from "react";
import LeaveRequest from "./LeaveRequest";
import { routerPaths } from "../../constants/paths";
import MenuNavbar from "../shared/components/MenuNavbar";
import LeaveSummary from "./LeaveSummary";

class LeaveIndex extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activePage: "summary",
      pages: [
        {
          name: "summary",
          label: "Summary",
          to: routerPaths.leaveSummary,
          component: () => <LeaveSummary />,
        },
      ],
    };
  }

  handlePageChange = (pageName) => {
    this.setState({ activePage: pageName });
    // this.props.history.push(this.state.pages.find((page) => page.name === pageName).to);
  };

  render() {
    const { activePage, pages } = this.state;

    return (
      <div>
        <MenuNavbar
          pages={pages}
          activePage={activePage}
          handlePageChange={this.handlePageChange}
        />
        <div className="page-container">
          {pages.find((page) => page.name === activePage).component()}
        </div>
      </div>
    );
  }
}

export default LeaveIndex;
