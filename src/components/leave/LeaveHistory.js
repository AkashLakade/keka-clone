import React, { Component } from "react";
import {
  Button,
  Dropdown,
  GridColumn,
  GridRow,
  Icon,
  Pagination,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHeader,
  TableHeaderCell,
  TableRow,
} from "semantic-ui-react";
import { leaveHistoryData } from "../../mocks/LeaveHistoryData";
import { LEAVE_HISTORY_COLUMNS } from "../../constants/columnsDef";
import EllipsesMenu from "../shared/components/EllipsesMenu";

const itemsPerPage = 10;

class LeaveHistory extends Component {
  state = {
    activePage: 1,
    sortedColumn: null,
    sortDirection: null,
    isDropdownVisible: {},
  };

  handlePageChange = (e, { activePage }) => {
    this.setState({ activePage });
  };

  handleSort = (clickedColumn) => () => {
    const { sortedColumn, sortDirection } = this.state;

    if (sortedColumn !== clickedColumn) {
      this.setState({
        sortedColumn: clickedColumn,
        sortDirection: "ascending",
      });
    } else {
      this.setState({
        sortDirection:
          sortDirection === "ascending" ? "descending" : "ascending",
      });
    }
  };

  handleEllipsisClick = (rowIndex) => {
    this.setState((prevState) => {
      const isDropdownVisible = { ...prevState.isDropdownVisible };
      isDropdownVisible[rowIndex] = !isDropdownVisible[rowIndex];
      return { isDropdownVisible };
    });
  };

  renderActionsCell = (item, rowIndex) => {
    const menuOptions = [
      {
        content: "View Request",
        onClick: () => console.log("View Request", rowIndex),
      },
    ];
    return <EllipsesMenu options={menuOptions} />;
  };

  render() {
    const { activePage, sortedColumn, sortDirection, isDropdownVisible } =
      this.state;

    const sortedData = [...leaveHistoryData].sort((a, b) => {
      const column = LEAVE_HISTORY_COLUMNS.find(
        (col) => col.label === sortedColumn,
      );
      const aValue =
        column && column.renderSortValue
          ? column.renderSortValue(a)
          : a[sortedColumn];
      const bValue =
        column && column.renderSortValue
          ? column.renderSortValue(b)
          : b[sortedColumn];

      if (aValue === undefined || bValue === undefined) {
        return 0;
      }
      if (aValue < bValue) {
        return sortDirection === "ascending" ? -1 : 1;
      }
      if (aValue > bValue) {
        return sortDirection === "ascending" ? 1 : -1;
      }
    });

    const startIdx = (activePage - 1) * itemsPerPage;
    const endIdx = activePage * itemsPerPage;

    const slicedData = sortedData.slice(startIdx, endIdx);

    return (
      <>
        <GridRow>
          <GridColumn>
            <p className="leave-header">Leave History</p>
          </GridColumn>
        </GridRow>
        <GridRow>
          <Table celled padded="very" sortable>
            <TableHeader>
              <TableRow>
                {LEAVE_HISTORY_COLUMNS.map((column, index) => (
                  <Table.HeaderCell
                    key={index}
                    sorted={
                      sortedColumn === column.label ? sortDirection : null
                    }
                    onClick={
                      column.sortable ? this.handleSort(column.label) : null
                    }
                  >
                    {column.label}
                  </Table.HeaderCell>
                ))}
                <TableHeaderCell>Actions</TableHeaderCell>
              </TableRow>
            </TableHeader>

            <TableBody>
              {slicedData.map((item, rowIndex) => (
                <Table.Row key={rowIndex}>
                  {LEAVE_HISTORY_COLUMNS.map((column, colIndex) => (
                    <Table.Cell key={colIndex}>
                      {column.label === "Actions"
                        ? column.renderCell(
                            item,
                            this.handleEllipsisClick,
                            isDropdownVisible,
                          )
                        : column.renderCell(item)}
                    </Table.Cell>
                  ))}
                  <Table.Cell>
                    {this.renderActionsCell(item, rowIndex)}
                  </Table.Cell>
                </Table.Row>
              ))}
            </TableBody>
            <TableFooter fullWidth>
              <TableRow>
                <Table.Cell colSpan="6" />
                <Pagination
                  firstItem={null}
                  lastItem={null}
                  pointing
                  secondary
                  totalPages={Math.ceil(leaveHistoryData.length / itemsPerPage)}
                  activePage={activePage}
                  onPageChange={this.handlePageChange}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </GridRow>
      </>
    );
  }
}

export default LeaveHistory;
