import React, { Component } from "react";
import AppLayout from "../layout/AppLayout";

class OrganizationIndex extends Component {
  render() {
    return (
      <AppLayout>
        <h2>Organization</h2>
      </AppLayout>
    );
  }
}

export default OrganizationIndex;
