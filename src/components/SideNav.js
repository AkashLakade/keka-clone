import React, { Component } from "react";
import { Menu, MenuItem, Sidebar } from "semantic-ui-react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-regular-svg-icons";
import {
  faBuilding,
  faHouse,
  faInbox,
  faIndianRupeeSign,
  faUsers,
} from "@fortawesome/free-solid-svg-icons";
import { withTranslation } from "react-i18next";
import { routerPaths } from "../constants/paths";
import { NavLink } from "react-router-dom";

class SideNav extends Component {
  state = { activeItem: "" };

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  render() {
    const { activeItem } = this.state;
    const { t } = this.props;
    return (
      <nav className="side-nav side-nav-sticky">
        <div className="side-nav-body min-h-100-50">
          <Sidebar
            as={Menu}
            animation="overlay"
            icon="labeled"
            inverted
            vertical
            visible={true}
            width="thin"
          >
            <MenuItem
              as={NavLink}
              name={t("home")}
              className="nav-item nav-link"
              active={activeItem === t("home")}
              onClick={this.handleItemClick}
              to={routerPaths.home}
            >
              <div className="sidebar-list-icon">
                <FontAwesomeIcon icon={faHouse} />
              </div>
              {t("home")}
            </MenuItem>
            <MenuItem
              as={NavLink}
              name={t("me")}
              className="nav-item nav-link"
              active={activeItem === t("me")}
              onClick={this.handleItemClick}
              to={routerPaths.me}
            >
              <div className="sidebar-list-icon">
                <FontAwesomeIcon icon={faUser} />
              </div>
              {t("me")}
            </MenuItem>
            <MenuItem
              as={NavLink}
              name={t("inbox")}
              className="nav-item nav-link"
              active={activeItem === t("inbox")}
              onClick={this.handleItemClick}
              to={routerPaths.inbox}
            >
              <div className="sidebar-list-icon">
                <FontAwesomeIcon icon={faInbox} />
              </div>
              {t("inbox")}
            </MenuItem>
            <MenuItem
              as={NavLink}
              name={t("myTeam")}
              className="nav-item nav-link"
              active={activeItem === t("myTeam")}
              onClick={this.handleItemClick}
              to={routerPaths.myTeam}
            >
              <div className="sidebar-list-icon">
                <FontAwesomeIcon icon={faUsers} />
              </div>
              {t("myTeam")}
            </MenuItem>
            <MenuItem
              as={NavLink}
              name={t("myFinance")}
              className="nav-item nav-link"
              active={activeItem === t("myFinance")}
              onClick={this.handleItemClick}
              to={routerPaths.myfinance}
            >
              <div className="sidebar-list-icon">
                <FontAwesomeIcon icon={faIndianRupeeSign} />
              </div>
              {t("myFinance")}
            </MenuItem>
            <MenuItem
              as={NavLink}
              name={t("organization")}
              className="nav-item nav-link"
              active={activeItem === t("organization")}
              onClick={this.handleItemClick}
              to={routerPaths.org}
            >
              <div className="sidebar-list-icon">
                <FontAwesomeIcon icon={faBuilding} />
              </div>
              {t("organization")}
            </MenuItem>
          </Sidebar>
        </div>
      </nav>
    );
  }
}

export default withTranslation("common")(SideNav);
