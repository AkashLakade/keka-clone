import React, { Component } from "react";

class EllipsesMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  toggleMenu = (e) => {
    e.stopPropagation();
    this.setState((prevState) => ({
      isOpen: !prevState.isOpen,
    }));
  };

  closeMenu = () => {
    this.setState({
      isOpen: false,
    });
  };

  componentDidMount() {
    document.addEventListener("click", this.closeMenu);
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.closeMenu);
  }

  render() {
    const { options } = this.props;
    const { isOpen } = this.state;

    return (
      <div
        className={`ellipses ${isOpen ? "active" : ""}`}
        onClick={this.toggleMenu}
      >
        <figure></figure>
        <figure className="middle"></figure>
        <figure></figure>
        <ul className={`dropdown ${isOpen ? "active" : ""}`}>
          {options.map((option, index) => (
            <li key={index} onClick={option.onClick && option.onClick}>
              {option.content}
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default EllipsesMenu;
