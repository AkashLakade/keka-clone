import React from "react";
import { Dropdown, Icon, Image } from "semantic-ui-react";

const ProfileDropdown = ({ profileName, profileImage }) => {
  const options = [
    { key: "profile", text: "Profile", icon: "user" },
    { key: "settings", text: "Settings", icon: "cog" },
    { key: "sign-out", text: "Sign Out", icon: "sign-out" },
  ];

  return (
    <Dropdown
      className="user-options"
      trigger={
        <>
          <span>{profileName}</span>
          <Icon name="angle down" />
          <div className="profile-picture-container">
            <Image className="profile-picture" avatar src={profileImage} />
          </div>
        </>
      }
      options={options}
      pointing="top right"
      icon={null}
    />
  );
};

export default ProfileDropdown;
