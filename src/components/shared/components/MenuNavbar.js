// MenuNavbar.js
import React from "react";
import { Menu } from "semantic-ui-react";

const MenuNavbar = ({ pages, activePage, handlePageChange }) => {
  return (
    <Menu pointing secondary stackable className="nav topnav">
      {pages.map((page, index) => (
        <Menu.Item
          key={index}
          name={page.label}
          active={page.name === activePage}
          onClick={() => handlePageChange(page.name)}
          // className={page.isFleetAdvise ? "font-weight" : ""}
        />
      ))}
    </Menu>
  );
};

export default MenuNavbar;
