import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { Image } from "semantic-ui-react";
import KekaLogo from "../../../assets/images/keka-logo-black.svg";

class SessionPrivacyPolicy extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { terms, privacyPolicy, t } = this.props;
    return (
      <React.Fragment>
        <div className="app-store-section mt-20 pt-2">
          <Image src={KekaLogo} />
          <div className="text-secondary text-x-small ms-3">
            {t("readAcceptTermLabel")}
            <a
              style={{ textDecoration: "underline", color: "unset" }}
              href={terms.url}
              rel="noopener noreferrer"
              target="_blank"
            >
              {t("termsLabel")}
            </a>
            {t("andLabel")}
            <a
              style={{ textDecoration: "underline", color: "unset" }}
              href={privacyPolicy.url}
              rel="noopener noreferrer"
              target="_blank"
            >
              {t("privacyPolicyLabel")}
            </a>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default withTranslation("common")(SessionPrivacyPolicy);
