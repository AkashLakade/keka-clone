import React from "react";
import { Image } from "semantic-ui-react";
import AppStoreImage from "../../../assets/images/app-store.svg";
import GooglePlayImage from "../../../assets/images/google-play.svg";

const SessionFooter = () => {
  return (
    <div className="app-store-section flex-gap-16 mt-30">
      <a
        href="https://itunes.apple.com/in/app/keka-hr/id1448024119?mt=8"
        target="_blank"
        rel="noreferrer"
      >
        <Image src={AppStoreImage} />
      </a>
      <a
        href="https://play.google.com/store/apps/details?id=com.keka.xhr&amp;hl=en_IN"
        target="_blank"
        rel="noreferrer"
      >
        <Image src={GooglePlayImage} />
      </a>
    </div>
  );
};

export default SessionFooter;
