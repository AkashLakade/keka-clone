import React, { Component } from "react";
import { Form, Grid, GridColumn, Search } from "semantic-ui-react";
import _ from "lodash";
import i18n from "../../../utils/i18n";

class SearchComponent extends Component {
  state = {
    isLoading: false,
    results: [],
    value: "",
  };

  handleResultSelect = (e, { result }) => {
    this.setState({ value: result.title });
  };

  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value });

    setTimeout(() => {
      const results = [
        { title: "Result 1", description: "Description 1" },
        { title: "Result 2", description: "Description 2" },
      ];

      this.setState({ isLoading: false, results });
    }, 300);
  };

  render() {
    const { isLoading, value, results } = this.state;

    return (
      <div className="ml-40 employee-search-typeahead">
        <div>
          <Search
            fluid
            input={{ icon: "search", iconPosition: "left" }}
            loading={isLoading}
            onResultSelect={this.handleResultSelect}
            onSearchChange={_.debounce(this.handleSearchChange, 500, {
              leading: true,
            })}
            results={results}
            value={value}
            placeholder={i18n.t("common:searchPlaceholder")}
          />
        </div>
      </div>
    );
  }
}

export default SearchComponent;
