import React from "react";
import { Form, FormField, Input } from "semantic-ui-react";

const FloatingLabelInput = ({
  size,
  label,
  required,
  value,
  onChange,
  ...rest
}) => {
  return (
    <Form size={size}>
      <FormField required={required}>
        <label>{label}</label>
        <Input value={value} onChange={onChange} {...rest} />
      </FormField>
    </Form>
  );
};

export default FloatingLabelInput;
