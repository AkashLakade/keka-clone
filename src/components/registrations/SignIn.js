import React, { Component } from "react";
import { Button, Image } from "semantic-ui-react";
import SignInBackground from "../../assets/images/login-image.jpg";
import { withTranslation } from "react-i18next";
import { Container } from "semantic-ui-react";
import FloatingLabelInput from "../shared/elements/FloatingLabelInput";
import SessionFooter from "../shared/components/SessionFooter";
import SessionPrivacyPolicy from "../shared/components/SessionPrivacyPolicy";
import { Redirect } from "react-router/cjs/react-router.min";
import { routerPaths } from "../../constants/paths";

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      authenticated: localStorage.getItem("authenticated") === "true",
    };
  }

  handleInputChange = (field, value) => {
    this.setState({ [field]: value });
  };

  handleSignIn = () => {
    const { email, password } = this.state;
    localStorage.setItem("user", JSON.stringify({ email, password }));
    localStorage.setItem("authenticated", "true");
    this.setState({ authenticated: true });

    alert("Signed in successfully!");
  };
  render() {
    const { t } = this.props;
    if (this.state.authenticated) {
      return this.props.history.push(routerPaths.me);
    }
    const { email, password } = this.state;
    return (
      <div className="session-container d-flex">
        <div className="position-relative bg-login w-100-500">
          <Image src={SignInBackground} />
        </div>
        <div className="login-container d-flex flex-column justify-content-between overflow-auto bg-primary">
          <div>
            <div className="h-50px login-empty-space" />
            <div className="login-header">{t("loginKekaLabel")}</div>
            <Container className="d-flex flex-column flex-gap-16">
              <FloatingLabelInput
                label={t("emailLbl")}
                size="big"
                required={true}
                value={email}
                onChange={(e) =>
                  this.handleInputChange("email", e.target.value)
                }
                placeholder={t("emailPlaceholder")}
              />
              <FloatingLabelInput
                size="big"
                required={true}
                label={t("passwordLbl")}
                value={password}
                onChange={(e) =>
                  this.handleInputChange("password", e.target.value)
                }
                type="password"
                placeholder={t("passwordPlaceholder")}
              />
              <Button size="big" fluid primary onClick={this.handleSignIn}>
                {t("signInLabel")}
              </Button>
            </Container>
          </div>
          <div>
            <SessionFooter />
            <SessionPrivacyPolicy
              terms={"https://www.keka.com/services-agreement"}
              privacyPolicy={"https://www.keka.com/privacy-policy"}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation("signin")(SignIn);
