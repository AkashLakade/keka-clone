import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import MenuNavbar from "../shared/components/MenuNavbar";
import AppLayout from "../layout/AppLayout";
import LeaveIndex from "../leave/LeaveIndex";
import AttendanceIndex from "../leave/AttendanceIndex";
import { routerPaths } from "../../constants/paths";

class EmployeeIndex extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activePage: "leave_summary",
      pages: [
        {
          name: "leave_summary",
          label: "Leave",
          to: routerPaths.leaveSummary,
          component: () => <LeaveIndex />,
        },
        {
          name: "attendance",
          label: "Attendance",
          to: routerPaths.attendance,
          component: () => <AttendanceIndex />,
        },
      ],
    };
  }

  handlePageChange = (pageName) => {
    this.setState({ activePage: pageName });
    // this.props.history.push(this.state.pages.find((page) => page.name === pageName).to);
  };

  render() {
    const { activePage, pages } = this.state;

    return (
      <AppLayout>
        <MenuNavbar
          pages={pages}
          activePage={activePage}
          handlePageChange={this.handlePageChange}
        />

        {pages.find((page) => page.name === activePage).component()}
      </AppLayout>
    );
  }
}

export default withRouter(EmployeeIndex);
