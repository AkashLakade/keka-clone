import React, { Component } from "react";
import AppLayout from "../layout/AppLayout";

class MyFinanceIndex extends Component {
  render() {
    return (
      <AppLayout>
        <h2>My Finance</h2>
      </AppLayout>
    );
  }
}

export default MyFinanceIndex;
