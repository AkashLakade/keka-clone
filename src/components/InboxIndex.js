import React, { Component } from "react";
import AppLayout from "./layout/AppLayout";

class InboxIndex extends Component {
  render() {
    return (
      <AppLayout>
        <h2>Inbox</h2>
      </AppLayout>
    );
  }
}

export default InboxIndex;
