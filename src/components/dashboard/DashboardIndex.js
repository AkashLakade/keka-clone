import React, { Component } from "react";
import AppLayout from "../layout/AppLayout";

class DashboardIndex extends Component {
  render() {
    return (
      <AppLayout>
        <h2>Dashboard Summary</h2>
      </AppLayout>
    );
  }
}

export default DashboardIndex;
