import React from "react";
import { ResponsivePie } from "@nivo/pie";

export const MyResponsivePie = ({
  data,
  topMargin,
  bottomMargin,
  customCenterTextLayer,
}) => {
  return (
    <ResponsivePie
      data={data}
      margin={{
        top: topMargin || 5,
        right: 0,
        bottom: bottomMargin || 5,
        left: 0,
      }}
      valueFormat=" >-"
      innerRadius={0.8}
      padAngle={2}
      cornerRadius={0}
      colors={({ data }) => data.color || "#808080"}
      borderWidth={1}
      borderColor={{
        from: "color",
        modifiers: [["darker", 0.2]],
      }}
      enableArcLinkLabels={false}
      arcLinkLabelsSkipAngle={10}
      arcLinkLabelsTextColor="#333333"
      arcLinkLabelsThickness={2}
      arcLinkLabelsColor={{ from: "color" }}
      enableArcLabels={false}
      arcLabelsSkipAngle={10}
      arcLabelsTextColor={{
        from: "color",
        modifiers: [["darker", 2]],
      }}
      legends={[]}
      activeOuterRadiusOffset={0}
      layers={["arcs", customCenterTextLayer]}
    />
  );
};
