import React from "react";
import { ResponsiveBar } from "@nivo/bar";

export const MyResponsiveBar = ({ data, usedKey, indexBy, maxValue }) => (
  <ResponsiveBar
    data={data}
    key={{ usedKey }}
    indexBy={indexBy}
    margin={{ top: 0, right: 0, bottom: 50, left: 0 }}
    padding={0.3}
    valueScale={{ type: "linear" }}
    indexScale={{ type: "band", round: true }}
    colors={{ scheme: "category10" }}
    borderColor={{
      from: "color",
      modifiers: [["darker", 1.6]],
    }}
    axisTop={null}
    axisRight={null}
    axisLeft={null}
    axisBottom={{
      tickSize: 0,
      tickPadding: 10,
      tickRotation: 0,
      legend: "",
      legendPosition: "middle",
      legendOffset: 32,
      truncateTickAt: 0,
    }}
    labelSkipWidth={12}
    labelSkipHeight={12}
    labelTextColor={{
      from: "color",
      modifiers: [["darker", 1.6]],
    }}
    legends={[]}
    enableGridY={false}
    enableLabel={false}
    maxValue={maxValue}
  />
);
