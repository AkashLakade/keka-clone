import React, { Component } from "react";
import AppLayout from "../layout/AppLayout";

class MyTeamIndex extends Component {
  render() {
    return (
      <AppLayout>
        <h2>My Team</h2>
      </AppLayout>
    );
  }
}

export default MyTeamIndex;
