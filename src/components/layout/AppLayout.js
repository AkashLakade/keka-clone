import React, { Component } from "react";
import TopNav from "../TopNav";
import SideNav from "../SideNav";

class AppLayout extends Component {
  render() {
    const { children } = this.props;

    return (
      <div>
        <TopNav />
        <div>
          <SideNav />
        </div>
        <div className="main-container">{children}</div>
      </div>
    );
  }
}

export default AppLayout;
