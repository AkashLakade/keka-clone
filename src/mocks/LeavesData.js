export const weeklyLeaveData = [
  {
    day: "Mon",
    value: 7,
  },
  {
    day: "Tue",
    value: 0,
  },
  {
    day: "Wed",
    value: 1,
  },
  {
    day: "Thu",
    value: 1.5,
  },
  {
    day: "Fri",
    value: 2,
  },
  {
    day: "Sat",
    value: 0,
  },
  {
    day: "Sun",
    value: 0,
  },
];

export const yearlyLeaveData = [
  {
    month: "Apr",
    value: 0,
  },
  {
    month: "May",
    value: 1,
  },
  {
    month: "Jun",
    value: 2.5,
  },
  {
    month: "Jul",
    value: 1,
  },
  {
    month: "June",
    value: 1,
  },
  {
    month: "Aug",
    value: 1,
  },
  {
    month: "Sep",
    value: 1,
  },
  {
    month: "Oct",
    value: 1,
  },
  {
    month: "Nov",
    value: 2,
  },
  {
    month: "Dec",
    value: 2,
  },
  {
    month: "Jan",
    value: 1,
  },
  {
    month: "Feb",
    value: 0,
  },
  {
    month: "Mar",
    value: 0,
  },
];

export const consumedLeaveTypesData = [
  {
    id: "Priviledge Leave",
    label: "Priviledge Leave",
    value: 8.5,
    color: "#2986CE",
  },
  {
    id: "General Leave",
    label: "General Leave",
    value: 4,
    color: "#534383",
  },
];

export const leaveBalance = [
  {
    id: "General Leave",
    label: "General Leave",
    value: 0,
    available: 0,
    consumed: 4,
    quota: 4,
    color: "#2986CE",
  },
  {
    id: "Privilege Leave",
    label: "Privilege Leave",
    value: 24.5,
    available: 24.5,
    consumed: 8.5,
    quota: 18,
    accrued: 18,
    carryover: 15,
    color: "#2986CE",
  },
  {
    id: "Unpaid Leave",
    label: "Unpaid Leave",
    value: "Infinity",
    available: "Infinity",
    consumed: 0,
    quota: "Infinity",
    color: "#2986CE",
  },
];
