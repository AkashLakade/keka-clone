export function hashRouteWithParams(route) {
  let stringParams = paramsString();
  if (stringParams != "") stringParams = `?${stringParams}`;
  return `/#${route}${stringParams}`;
}

export function paramsString() {
  return (window.location.href.split("?") || [])[1] || "";
}
