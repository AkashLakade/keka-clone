import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import moment from "moment";
import enCommon from "../locales/en/common.json";
import frCommon from "../locales/fr/common.json";
import enSignIn from "../locales/en/signin.json";
import frSignIn from "../locales/fr/signin.json";
import { selectLanguage } from "../helpers/languageHelper";

i18n.use(initReactI18next).init({
  lng: "en",
  ns: ["common", "signin"],
  defaultNS: "common",
  resources: {
    en: {
      common: enCommon,
      signin: enSignIn,
    },
    fr: {
      common: frCommon,
      signin: frSignIn,
    },
  },
  fallbackLng: "en",
  interpolation: {
    escapeValue: false,
    format: function (value, format, lng) {
      if (value instanceof Date) {
        moment.locale(lng);
        return moment(value).locale(lng).format(format);
      }
      return value;
    },
  },
});

export default i18n;
