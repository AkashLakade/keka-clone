import React from "react";
import StylesheetInjector from "./configs/StylesheetInjector";
import Router from "./Router";
import { I18nextProvider } from "react-i18next";
import i18n from "./utils/i18n";
import "./assets/stylesheet/keka.scss";

export default class App extends React.Component {
  async componentDidMount() {
    StylesheetInjector();
  }

  render() {
    return (
      <I18nextProvider i18n={i18n}>
        <Router />
      </I18nextProvider>
    );
  }
}
