let loadStylesheet = (url, callback) => {
  let sheet = document.createElement("link");
  sheet.rel = "stylesheet";
  sheet.href = url;
  sheet.type = "text/css";
  sheet.onload = () => callback();
  document.head.appendChild(sheet);
};

export default function StylesheetInjector() {
  loadStylesheet(`${window.REACT_APP_PUBLIC_URL}/css/keka.css`, () => {
    document.querySelector("#splash").style.display = "none";
    document.querySelector("#root").style.display = "block";
  });
}
