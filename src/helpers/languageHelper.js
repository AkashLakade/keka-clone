export function selectLanguage() {
  const storedLanguage = localStorage.getItem("language");

  if (storedLanguage) {
    return storedLanguage;
  }

  const browserLanguage = navigator.language || navigator.userLanguage;

  let selectedLanguage;

  switch (true) {
    case browserLanguage.includes("fr"):
      selectedLanguage = "fr";
      break;

    default:
      selectedLanguage = "en";
      break;
  }
  localStorage.setItem("language", selectedLanguage);

  return selectedLanguage;
}
