export function capitalCase(string) {
  if (string == null) return "";

  return string.charAt(0).toUpperCase() + string.toLowerCase().slice(1);
}
